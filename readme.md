# [Genealogy Viewer](http://macalat.github.com/genealogy)

## What it is
* This is a simple tool to view a genealogy tree for an MLM binary system.
* Shows the number of downlines of each member.
*	Has capability to filter down the list to a specific downline's genealogy tree. 
* A personal practice project that anyone can freely use if you wish to do so. A little credit will not hurt though :)

## What it is not
* It is not an MLM software replacer.
* It is not capable of tracking members joined date.
* It is not dynamic, data will have to be typed manually.
* It is not capable of computing anything else aside from the number of people below the member's left and right side.

##Credits

* The core of this project was created using Basic Primitives' [Genealogy Organization Chart](http://basicprimitives.com)

* [Fuse.js](http://kiro.me/projects/fuse.html) for awesome fuzzy searching tool.

* [faceScroll.js](http://www.dynamicdrive.com/dynamicindex11/facescroll/) for fantastic scrollbar

* [icheck](http://damirfoy.com/iCheck/) for the custom radio button.