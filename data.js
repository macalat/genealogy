/*****************************
	COLORS Available
*****************************/
/* AliceBlue, AntiqueWhite, Aqua, Aquamarine, Azure
Beige, Bisque, Black, BlanchedAlmond, Blue, BlueViolet, Brown, BurlyWood, Bronze
CadetBlue, ChartReuse, Chocolate, Coral, CornflowerBlue, Cornsilk, Crimson, Cyan, DarkBlue, DarkCyan, DarkGoldenrod, DarkGray, DarkGreen, DarkKhaki, DarkMagenta, DarkOliveGreen, Darkorange, DarkOrchid, DarkRed, DarkSalmon, DarkSeaGreen, DarkSlateBlue, DarkSlateGray, DarkTurquoise, DarkViolet, DeepPink, DeepSkyBlue, DimGray, DodgerBlue
FireBrick, FloralWhite, ForestGreen, Fuchsia
Gainsboro, GhostWhite, Gold, Goldenrod, Gray, Green, GreenYellow
Honeydew, Hotpink
IndianRed, Indigo, Ivory, Khaki
Lavender, LavenderBlush, Lawngreen, Lemonchiffon, LightBlue, LightCoral, LightCyan, LightGoldenrodYellow
LightGray, LightGreen, LightPink, LightSalmon, LightSeaGreen, LightSkyBlue, LightSlateGray, LightSteelBlue
LightYellow, Lime, Limegreen, Linen
Magenta, Maroon, MediumAquamarine, MediumBlue, MediumOrchid, MediumPurple, MediumSeaGreen, MediumSlateBlue
MediumSpringGreen, MediumTurquoise, MediumVioletRed, MidnightBlue, MintCream, MistyRose, Moccasin
NavajoWhite, Navy
Oldlace, Olive, Olivedrab, Orange, OrangeRed, Orchid
PaleGoldenRod, PaleGreen, PaleTurquoise, PaleVioletRed, Papayawhip, Peachpuff, Peru, Pink, Plum, PowderBlue, Purple
Red, RosyBrown, RoyalBlue
SaddleBrown, Salmon, SandyBrown, SeaGreen, Seashell, Sienna, Silver, SkyBlue, SlateBlue, SlateGray, Snow, SpringGreen, SteelBlue
Tan, Teal, Thistle, Tomato, Turquoise
Violet
Wheat, White, WhiteSmoke
Yellow, YellowGreen

*/

var data = {
	name: "Mark Jayson Mandap",
	dealerNo: "27814-01",
	itemTitleColor: "",
	items: [
		{
			name: "Mark Jayson Mandap",
			dealerNo: "27814-02",
			itemTitleColor: "",
			items: [
				{
					name: "Mark Jayson Mandap",
					dealerNo: "27814-03",
					itemTitleColor: "DeepSkyBlue",
					items: [
						{
							name: "Glen Flor San Pedro",
							dealerNo: "27899-01",
							itemTitleColor: "SteelBlue",
							items: [
								{
									name: "Johnson Hernandez",
									dealerNo: "27901-01",
									itemTitleColor: "SteelBlue",
									items: [
										{
											name: "Johnson Hernandez",
											dealerNo: "27901-02",
											itemTitleColor: "SteelBlue",
											items: [
												{
													name: "Mary Ann Palileo",
													dealerNo: "28826-01",
													itemTitleColor: "SteelBlue"
												},
												{
													name: "Lito Sancho",
													dealerNo: "28825-01",
													itemTitleColor: "SteelBlue",
													items: [
														{
															name: "Lito Sancho",
															dealerNo: "28825-02",
															itemTitleColor: "SteelBlue",
															items: [
																{
																	name: "Teodoro Loberas Jr",
																	dealerNo: "29644-01",
																	itemTitleColor: "SteelBlue",
																	items: [
																		{
																			name: "Richmond Coralde",
																			dealerNo: "29646-01",
																			itemTitleColor: "SteelBlue"
																		},
																		{
																			name: "",
																			dealerNo: ""
																		}
																	]
																},
																{
																	name: "",
																	dealerNo: ""
																}
															]
														},
														{
															name: "",
															dealerNo: ""
														}
													]
												}
											]
										},
										{
											name: "Johnson Hernandez",
											dealerNo: "27901-03",
											itemTitleColor: "SteelBlue"
										}
									]
								},
								{
									name: "Glen Flor San Pedro",
									dealerNo: "27899-02",
									itemTitleColor: "SteelBlue",
									items: [
										{
											name: "Glen Flor San Pedro",
											dealerNo: "27899-03",
											itemTitleColor: "SteelBlue",
											items: [
												{
													name: "Glen Flor Belo",
													dealerNo: "28941-01",
													itemTitleColor: "SteelBlue",
													items: [
														{
															name: "Joylen Belo",
															dealerNo: "28943-01",
															itemTitleColor: "SteelBlue",
															items: [
																{
																	name: "Glenton Belo",
																	dealerNo: "28944-01",
																	itemTitleColor: "SteelBlue",
																	items: [
																		{
																			name: "Jennifer San Pedro",
																			dealerNo: "300065-01",
																			itemTitleColor: "SteelBlue",
																			items: [
																				{
																					name: "Pio San Pedro",
																					dealerNo: "32277-01",
																					itemTitleColor: "SteelBlue",
																					items: [
																						{
																							name: "Pio San Pedro",
																							dealerNo: "32277-02",
																							itemTitleColor: "SteelBlue",
																							items: [
																								{
																									name: "Pio San Pedro",
																									dealerNo: "32277-03",
																									itemTitleColor: "SteelBlue",
																									items: [
																									  {
																									    name: "Cindy Badialan ",
																									    dealerNo: "33098-01",
																									    itemTitleColor: "SteelBlue"
																									  },
																									  {
																									    name: "",
																									    dealerNo: "",
																									    itemTitleColor: ""
																									  }
																									]
																								},
																								{
																									name: "Pio San Pedro",
																									dealerNo: "32283-01",
																									itemTitleColor: "SteelBlue"
																								}
																							]
																						},
																						{
																							name: "",
																							dealerNo: ""
																						}
																					]
																				},
																				{
																					name: "",
																					dealerNo: ""
																				}
																			]
																		},
																		{
																			name: "",
																			dealerNo: ""
																		}
																	]
																},
																{
																	name: "",
																	dealerNo: ""
																}
															]
														},
														{
															name: "",
															dealerNo: ""
														}
													]
												},
												{
													name: "",
													dealerNo: ""
												}
											]
										},
										{
											name: "",
											dealerNo: ""
										}
									]
								}
							]
						},
						{
							name: "",
							dealerNo: ""
						}
					]
				},
				{
					name: "Isabelita Gabriel",
					dealerNo: "30002-01",
					itemTitleColor: "YellowGreen",
					items: [
						{
							name: "Jeffrey Calalang",
							dealerNo: "31714-01",
							itemTitleColor: "SeaGreen",
							items: [
								{
									name: "Mary Ann Purisima",
									dealerNo: "31719-01",
									itemTitleColor: "SeaGreen",
									items: [
										{
											name: "Mary Ann Purisima",
											dealerNo: "31719-02",
											itemTitleColor: "SeaGreen",
											items: [
												{
													name: "Mary Ann Purisima",
													dealerNo: "31719-03",
													itemTitleColor: "SeaGreen"
												},
												{
													name: "",
													dealerNo: ""
												}
											]
										},
										{
											name: "",
											dealerNo: ""
										}
									]
								},
								{
									name: "Jeffrey Calalang",
									dealerNo: "31714-02",
									itemTitleColor: "SeaGreen",
									items: [
										{
											name: "Juanino Calalang",
											dealerNo: "34264-01",
											itemTitleColor: "SeaGreen",
											items: [
												{
													name: "Juanino Calalang",
													dealerNo: "34264-02",
													itemTitleColor: "SeaGreen",
													items: [
														{
															name: "Juanino Calalang",
															dealerNo: "34264-03",
															itemTitleColor: "SeaGreen"
														},
														{
															name: "",
															dealerNo: "",
															itemTitleColor: ""
														}
													]
												},
												{
													name: "",
													dealerNo: "",
													itemTitleColor: ""
												}
											]
										},
										{
											name: "",
											dealerNo: "",
											itemTitleColor: ""
										}
									]
								}
							]
						},
						{
							name: "",
							dealerNo: ""
						}
					]
				}
			]
		},
		{
			name: "",
			dealerNo: "",
			itemTitleColor: "",
			items: [
				{
					name: "",
					dealerNo: ""
				},
				{
					name: "",
					dealerNo: ""
				}
			]
		}
	]
};